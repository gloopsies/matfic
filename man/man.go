package man

import (
	"bytes"
	"os/exec"
)

func Man(stranica string) (*bytes.Buffer, error) {
	cmd := exec.Command("man", stranica)
	cmd.Env = append(cmd.Env, "MANWIDTH=80")
	var out bytes.Buffer
	cmd.Stdout = &out

	err := cmd.Run()

	return &out, err
}
