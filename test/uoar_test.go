package test

import (
	"matf-bot/uoar"
	"testing"
)

func TestSaberiZAGranice(t *testing.T) {
	_, err := uoar.SaberiZA(256, 5)
	if err == nil {
		t.Errorf("Function didn't return error\n")
		return
	}
	if err.Error() != "a je izvan granica" {
		t.Errorf("Function returned wrong error: %s\n", err.Error())
		return
	}

	_, err = uoar.SaberiZA(1, 257)
	if err == nil {
		t.Errorf("Function didn't return error\n")
	}
	if err.Error() != "b je izvan granica" {
		t.Errorf("Function returned wrong error: %s\n", err.Error())
		return
	}
}

func TestSaberiZA(t *testing.T) {
	expected := "3 + 5\n"
	expected += "a: 00000011\n"
	expected += "b: 00000101\n"
	expected += "-----------\n"
	expected += "   00001000\n"

	rez, err := uoar.SaberiZA(3, 5)
	if err != nil {
		t.Errorf("Function returned error: %s\n", err.Error())
		return
	}

	if rez != expected {
		t.Errorf("Function returned : %s\n\nExpected: %s\n", rez, expected)
		return
	}
}
