package urm

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

const MaxInstructions = 10000
const MaxRegisters = 1000

type prog struct {
	instruction rune
	arguments   [3]int
}

type Urm struct {
	registers [MaxRegisters]int
	regNum    int
	program   []prog
}

func Init() *Urm {
	var urm = Urm{
		registers: [MaxRegisters]int{0},
		regNum:    0,
	}

	return &urm
}

func (urm *Urm) setReg(pos int, val int) error {
	if pos >= MaxRegisters || pos < 0 {
		return errors.New(fmt.Sprintf("Registar %d izvan opsega", pos+1))
	}

	urm.registers[pos] = val
	if pos > urm.regNum {
		urm.regNum = pos
	}

	return nil
}

func (urm *Urm) printReg() {
	for _, num := range urm.registers[:urm.regNum+1] {
		fmt.Printf("%d ", num)
	}

	fmt.Println()
}

func (urm *Urm) GetReg() string {
	ret := ""
	for _, num := range urm.registers[:urm.regNum+1] {
		ret = fmt.Sprintf("%s%d ", ret, num)
	}

	return ret
}

func (urm *Urm) Parse(input string) error {
	// Set input registers
	if strings.Index(input, "\n") == -1 {
		return errors.New("niste uneli instrukcije")
	}

	eol := strings.Index(input, "\n")
	if eol == -1 {
		return errors.New("niste uneli instrukcije")
	}

	in := input[:eol]
	for i, num := range strings.Fields(in) {
		n, err := strconv.Atoi(num)
		if err != nil {
			return errors.New("loš unos podataka")
		}
		err = urm.setReg(i, n)
		if err != nil {
			return err
		}
	}

	if input[len(input)-1] != '\n' {
		input = input + "\n"
	}

	input = input[strings.Index(input, "\n")+1:]

	//Go through lines
	var str string
	var line = 0
	for loc := strings.Index(input, "\n"); loc != -1; loc = strings.Index(input, "\n") {
		line++
		str = strings.Trim(input[:loc], " \t")
		input = input[loc+1:]

		if strings.HasPrefix(str, "//") {
			continue
		}

		str = strings.Replace(str, " ", "", -1)

		regex, err := regexp.Compile(`^[0-9]+:`)
		if err != nil {
			return err
		}
		str = regex.ReplaceAllString(str, "")

		mapArgs := map[rune]int{
			'Z': 1,
			'S': 1,
			'J': 3,
			'T': 2,
		}

		instruction := rune(strings.ToUpper(str)[0])
		args := strings.Split(str[2:len(str)-1], ",")
		if nArgs, ok := mapArgs[instruction]; !ok {
			return errors.New(
				fmt.Sprintf("Linija %d: nepoznata instrukcija %c", line, instruction))
		} else if nArgs != len(args) {
			return errors.New(
				fmt.Sprintf(
					"Linija %d: pogrešan broj argumenata za instrukciju %c; očekivano %d, dobijeno %d",
					line, instruction, nArgs, len(args)))
		}

		arguments := [3]int{}
		for i, arg := range args {
			var err error
			arguments[i], err = strconv.Atoi(arg)
			if err != nil {
				return err
			}
		}

		urm.program = append(urm.program, prog{
			instruction: instruction,
			arguments:   arguments,
		})

	}

	return nil

}

func (urm *Urm) Run() error {
	instruction := 0

	br := 0
	for br = 0; br < MaxInstructions && instruction < len(urm.program); br++ {
		i := urm.program[instruction]
		switch i.instruction {
		case 'Z':
			err := urm.setReg(i.arguments[0]-1, 0)
			if err != nil {
				return err
			}
		case 'S':
			err := urm.setReg(i.arguments[0]-1, urm.registers[i.arguments[0]-1]+1)
			if err != nil {
				return err
			}
		case 'J':
			if urm.registers[i.arguments[0]-1] == urm.registers[i.arguments[1]-1] {
				instruction = i.arguments[2] - 1
				continue
			}
		case 'T':
			err := urm.setReg(i.arguments[1]-1, urm.registers[i.arguments[0]-1])
			if err != nil {
				return err
			}
		}

		instruction++
	}

	return nil
}
