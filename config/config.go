package config

import (
	"encoding/json"
	"io/ioutil"
)

var Config config

const configFile = "./config.json"

type config struct {
	Prefix  string `json:"prefix"`
	Color   int    `json:"color"`
	Token   string `json:"token"`
	GuildID string `json:"guild_id"`
	Sreda   struct {
		Id   string `json:"id"`
		Role string `json:"role"`
	} `json:"sreda"`
	Roles []struct {
		Name      string `json:"name"`
		MessageID string `json:"message_id"`
		Roles     []struct {
			Name   string `json:"name"`
			RoleID string `json:"role_id"`
		} `json:"roles"`
	} `json:"roles"`
	RolesChannel  string `json:"roles_channel"`
	QuotesChannel string `json:"quotes_channel"`
}

func (conf *config) Read() error {
	data, err := ioutil.ReadFile(configFile)
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, conf)
	if err != nil {
		return err
	}
	return nil
}

func (conf *config) Save() error {
	data, err := json.MarshalIndent(conf, "", "\t")
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(configFile, data, 0644)
	if err != nil {
		return err
	}
	return nil
}
