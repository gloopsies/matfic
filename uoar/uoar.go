package uoar

import (
	"errors"
	"fmt"
	"strconv"
)

func SaberiZA(a int, b int) (string, error) {
	rez := strconv.Itoa(a) + " + " + strconv.Itoa(b) + "\n"
	if a >= 256 || a < -256 {
		return "", errors.New("a je izvan granica")
	}

	if b >= 256 || b < -256 {
		return "", errors.New("b je izvan granica")
	}

	rez += fmt.Sprintf("a: %08b\n", a)
	rez += fmt.Sprintf("b: %08b\n", b)
	rez += "-----------\n"

	if a+b <= 256 {
		rez += fmt.Sprintf("   %08b\n", a+b)
	} else {
		rez += fmt.Sprintf("  %09b Prekoračenje!\n", a+b)
	}

	return rez, nil
}
