package roles

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"os"

	. "matf-bot/config"
)

var emojis = []string{
	"0️⃣", "1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣",
	"🇦", "🇧", "🇨", "🇩", "🇪", "🇫",
}

var emojiIndexes = map[string]int{
	"0️⃣": 0, "1️⃣": 1, "2️⃣": 2, "3️⃣": 3, "4️⃣": 4, "5️⃣": 5, "6️⃣": 6, "7️⃣": 7, "8️⃣": 8, "9️⃣": 9,
	"🇦": 10, "🇧": 11, "🇨": 12, "🇩": 13, "🇪": 14, "🇫": 15,
}

func SendMessages(session *discordgo.Session) {
	for i, roles := range Config.Roles {
		message := ""
		for j, role := range roles.Roles {
			message += emojis[j] + "   " + role.Name + "\n"
		}

		msg := discordgo.MessageEmbed{
			Title:       roles.Name,
			Description: message,
		}
		if len(roles.MessageID) == 0 {
			embed, err := session.ChannelMessageSendEmbed(Config.RolesChannel, &msg)

			if err != nil {
				fmt.Println(err.Error())
				os.Exit(1)
			}

			Config.Roles[i].MessageID = embed.ID
		} else {
			err := session.MessageReactionsRemoveAll(Config.RolesChannel, roles.MessageID)
			if err != nil {
				fmt.Println(err.Error())
				os.Exit(1)
			}

			_, err = session.ChannelMessageEditEmbed(Config.RolesChannel, roles.MessageID, &msg)
			if err != nil {
				fmt.Println(err.Error())
				os.Exit(1)
			}
		}

		for index := 0; index < len(roles.Roles); index++ {
			err := session.MessageReactionAdd(Config.RolesChannel, Config.Roles[i].MessageID, emojis[index])
			if err != nil {
				fmt.Println(err.Error())
				os.Exit(1)
			}
		}
	}

	err := Config.Save()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}

func hasRole(member *discordgo.Member, role string) bool {
	for _, currRole := range member.Roles {
		if currRole == role {
			return true
		}
	}

	return false
}

func HandleReactionAdd(botID string) func(session *discordgo.Session, reaction *discordgo.MessageReactionAdd) {
	return func(session *discordgo.Session, reaction *discordgo.MessageReactionAdd) {
		if reaction.UserID == botID {
			return
		}

		for _, roles := range Config.Roles {
			if roles.MessageID != reaction.MessageID {
				continue
			}

			if err := session.MessageReactionRemove(reaction.ChannelID, reaction.MessageID, reaction.Emoji.APIName(), reaction.UserID); err != nil {
				fmt.Println(err.Error())
			}

			if _, ok := emojiIndexes[reaction.Emoji.Name]; !ok {
				break
			}

			if emojiIndexes[reaction.Emoji.Name] < len(roles.Roles) {
				member, err := session.GuildMember(reaction.GuildID, reaction.UserID)
				if err != nil {
					fmt.Println(err.Error())
					return
				}

				role := roles.Roles[emojiIndexes[reaction.Emoji.Name]].RoleID
				if hasRole(member, role) {
					err = session.GuildMemberRoleRemove(reaction.GuildID, reaction.UserID, role)
					if err != nil {
						fmt.Println(err.Error())
						return
					}
				} else {
					err = session.GuildMemberRoleAdd(reaction.GuildID, reaction.UserID, role)
					if err != nil {
						fmt.Println(err.Error())
						return
					}
				}
				if err != nil {
					fmt.Println(err.Error())
					return
				}
			}

			break
		}
	}
}
