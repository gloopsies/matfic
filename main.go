package main

import (
	"errors"
	"fmt"

	"os"
	"strconv"
	"strings"
	"time"

	"math/rand"
	"net/url"

	"github.com/alwindoss/morse"
	"github.com/bwmarrin/discordgo"

	"matf-bot/man"
	"matf-bot/roles"
	"matf-bot/uoar"
	"matf-bot/urm"

	. "matf-bot/config"
)

var bot discordgo.User
var h morse.Hacker

func main() {
	rand.Seed(time.Now().Unix())

	if err := Config.Read(); err != nil {
		fmt.Println(err.Error())
		return
	}

	session, err := discordgo.New("Bot " + Config.Token)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	botUser, err := session.User("@me")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	bot = *botUser

	h = morse.NewHacker()

	session.AddHandler(reactionAddHandler)
	session.AddHandler(reactionRemoveHandler)
	session.AddHandler(guildJoinedHandler)
	session.AddHandler(messageHandler)

	if err := session.Open(); err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("Running bot...")
	<-make(chan struct{})

	err = session.Close()
	fmt.Println(err.Error())
	return
}

func reactionAddHandler(session *discordgo.Session, reaction *discordgo.MessageReactionAdd) {
	if reaction.UserID == bot.ID {
		return
	}

	roles.HandleReactionAdd(bot.ID)(session, reaction)

	reactionPrase(session, reaction)

	reactionPin(session, reaction)
}

func reactionRemoveHandler(session *discordgo.Session, reaction *discordgo.MessageReactionRemove) {
	if reaction.UserID == bot.ID {
		return
	}

	reactionUnpin(session, reaction)
}

func reactionPrase(session *discordgo.Session, reaction *discordgo.MessageReactionAdd) {
	if reaction.Emoji.Name != "🔥" {
		return
	}

	msg, err := session.ChannelMessage(reaction.ChannelID, reaction.MessageID)
	if err != nil {
		fmt.Println(err.Error())
	}

	if msg.Author.ID == bot.ID && msg.Content == "🐖" {
		for _, react := range msg.Reactions {
			if react.Emoji.Name == "🔥" && react.Count == 4 {
				_, err := session.ChannelMessageEdit(msg.ChannelID, msg.ID, "🥓")
				if err != nil {
					fmt.Println(err.Error())
				}
			}
		}
	}

}

func reactionPin(session *discordgo.Session, reaction *discordgo.MessageReactionAdd) {
	if reaction.Emoji.Name != "📌" {
		return
	}

	msg, err := session.ChannelMessage(reaction.ChannelID, reaction.MessageID)
	if err != nil {
		return
	}
	for _, react := range msg.Reactions {
		if react.Emoji.Name == "📌" {
			if react.Count > 1 {
				return
			}
			break
		}
	}

	if err := session.ChannelMessagePin(reaction.ChannelID, reaction.MessageID); err != nil {
		fmt.Println(err.Error())
		return
	}

	if err := session.MessageReactionAdd(reaction.ChannelID, reaction.MessageID, "✅"); err != nil {
		fmt.Println(err.Error())
		return
	}
}

func reactionUnpin(session *discordgo.Session, reaction *discordgo.MessageReactionRemove) {
	if reaction.Emoji.Name != "📌" {
		return
	}

	msg, err := session.ChannelMessage(reaction.ChannelID, reaction.MessageID)
	if err != nil {
		return
	}

	for _, react := range msg.Reactions {
		if react.Emoji.Name == "📌" {
			if react.Count > 0 {
				return
			}
			break
		}
	}

	if err := session.ChannelMessageUnpin(reaction.ChannelID, reaction.MessageID); err != nil {
		fmt.Println(err.Error())
	}

	if err := session.MessageReactionRemove(reaction.ChannelID, reaction.MessageID, "✅", bot.ID); err != nil {
		fmt.Println(err.Error())
		return
	}
}

func guildJoinedHandler(session *discordgo.Session, _ *discordgo.GuildCreate) {
	roles.SendMessages(session)

	println("joined")
}

type messageFunc func(session *discordgo.Session, message *discordgo.MessageCreate, prefix string) error

func sendMeme(name string) messageFunc {
	return func(session *discordgo.Session, message *discordgo.MessageCreate, prefix string) error {
		if len(message.Content) > len(prefix) {
			return nil
		}

		reader, err := os.Open("memes/" + name)
		if err != nil {
			return err
		}

		_, err = session.ChannelFileSend(message.ChannelID, name, reader)

		return err
	}
}

func prase(session *discordgo.Session, message *discordgo.MessageCreate, _ string) error {
	msg, err := session.ChannelMessageSend(message.ChannelID, "🐖")
	if err != nil {
		return err
	}

	err = session.MessageReactionAdd(msg.ChannelID, msg.ID, "🔥")
	return err
}

func morseCode(session *discordgo.Session, message *discordgo.MessageCreate, prefix string) error {
	msg := message.Content[len(prefix):]
	decoded, err := h.Decode(strings.NewReader(msg))
	if err != nil {
		return err
	}

	if len(decoded) == 0 {
		decoded, err = h.Encode(strings.NewReader(msg))
		if err != nil {
			return err
		}
	}

	_, err = session.ChannelMessageSend(message.ChannelID, strings.ToLower(string(decoded)))
	return err
}

func urmRun(session *discordgo.Session, message *discordgo.MessageCreate, prefix string) error {
	if len(message.Content) <= len(prefix) {
		_, err := session.ChannelMessageSend(message.ChannelID, "```\nNiste uneli instrukcije```")
		return err
	}

	u := urm.Init()
	err := u.Parse(strings.Replace(message.Content[len(prefix):], "```", "", -1))
	if err != nil {
		_, err = session.ChannelMessageSend(message.ChannelID, "```"+err.Error()+"```")
		return err
	}

	err = u.Run()
	if err != nil {
		_, err = session.ChannelMessageSend(message.ChannelID, "```"+err.Error()+"```")
		return err
	}

	_, err = session.ChannelMessageSend(message.ChannelID, "```"+u.GetReg()+"```")
	return err
}

func uoarFunc(session *discordgo.Session, message *discordgo.MessageCreate, prefix string) error {
	fields := strings.Fields(message.Content[len(prefix)-1:])

	if len(fields) != 3 {
		_, _ = session.ChannelMessageSend(message.ChannelID, "```Pogrešan broj argumenata```")
		return errors.New("pogrešan broj argumenata")
	}

	a, err := strconv.Atoi(fields[1])
	if err != nil {
		_, _ = session.ChannelMessageSend(message.ChannelID, "```Prvi argument nije broj```")
		return err
	}
	b, err := strconv.Atoi(fields[2])
	if err != nil {
		_, _ = session.ChannelMessageSend(message.ChannelID, "```Drugi argument nije broj```")
		return err
	}

	switch fields[0] {
	case "saberiZA":
		{
			rez, err := uoar.SaberiZA(a, b)
			if err != nil {
				_, _ = session.ChannelMessageSend(message.ChannelID, "```"+err.Error()+"```")
				return err
			} else {
				_, err = session.ChannelMessageSend(message.ChannelID, "```"+rez+"```")
				return err
			}
		}
	default:
		return nil
	}
}

func manPoruka(session *discordgo.Session, message *discordgo.MessageCreate, prefix string) error {
	m, err := man.Man(message.Content[len(prefix):])
	if err != nil {
		poruka, err := session.ChannelMessageSend(message.ChannelID, "```Nepoznata stranica "+message.Content[len(prefix):]+"```")

		err = session.MessageReactionAdd(poruka.ChannelID, poruka.ID, "👉")
		if err != nil {
			return err
		}

		err = session.MessageReactionAdd(poruka.ChannelID, poruka.ID, "👈")
		if err != nil {
			return err
		}

		err = session.MessageReactionAdd(poruka.ChannelID, poruka.ID, "\U0001F97A")
		if err != nil {
			return err
		}
		return nil
	}

	_, err = session.ChannelFileSend(message.ChannelID, "man "+message.Content[len(prefix):]+".txt", m)

	return err
}

func citat(session *discordgo.Session, message *discordgo.MessageCreate, _ string) error {
	citati, err := session.ChannelMessages(Config.QuotesChannel, 100, "", "", "")
	if err != nil {
		return err
	}

	citat := citati[rand.Intn(len(citati))]

	poruka := ""
	for _, img := range citat.Attachments {
		poruka += img.URL + "\n"
	}
	poruka += citat.Content

	_, err = session.ChannelMessageSend(message.ChannelID, poruka)
	return err
}

func lmgtfy(domain string) messageFunc {
	return func(session *discordgo.Session, message *discordgo.MessageCreate, prefix string) error {
		if len(prefix) >= len(message.Content) {
			_, err := session.ChannelMessageSend(message.ChannelID, "```\nMolim Vas unesite pojam za pretragu\n```")
			return err
		}
		_, err := session.ChannelMessageSend(message.ChannelID, "https://"+domain+"?q="+url.QueryEscape(message.Content[len(prefix):]))
		return err
	}
}

type prefixes struct {
	prefix   string
	short    bool
	function messageFunc
}

func messageHandler(session *discordgo.Session, message *discordgo.MessageCreate) {
	if message.Type == discordgo.MessageTypeChannelPinnedMessage {
		if err := session.ChannelMessageDelete(message.ChannelID, message.ID); err != nil {
			fmt.Println(err.Error())
		}
		return
	}

	if message.Author.ID == bot.ID {
		return
	}

	var commands = []prefixes{
		{"smederevo", false, sendMeme("smederevo.png")},
		{"mirijevo", false, sendMeme("mirijevo.png")},
		{"help", false, sendMeme("help.png")},
		{"prase", false, prase},
		{"krme", true, prase},
		{"svinja", false, prase},
		{"morse", true, morseCode},
		{"urm", false, urmRun},
		{"uoar", true, uoarFunc},
		{"man", true, manPoruka},
		{"citat", false, citat},
		{"lmgtfy", true, lmgtfy("lmgtfy.app/")},
		{"lmg", true, lmgtfy("letmegooglethat.app/")},
		{"google", false, lmgtfy("google.com/search")},
	}

	if strings.HasPrefix(message.Content, Config.Prefix) {
		for _, command := range commands {
			if strings.HasPrefix(message.Content, Config.Prefix+command.prefix) {
				var err = command.function(session, message, Config.Prefix+command.prefix+" ")

				if err != nil {
					println(err.Error())
				}
				return
			}
		}
	}

	for _, command := range commands {
		if strings.HasPrefix(message.Content, command.prefix+" ") && command.short {
			var err = command.function(session, message, command.prefix+" ")

			if err != nil {
				println(err.Error())
			}
			return
		}
	}

	return
}
