module matf-bot

go 1.16

require (
	github.com/alwindoss/morse v1.0.1
	github.com/bwmarrin/discordgo v0.23.2
	github.com/go-co-op/gocron v1.9.0
)
